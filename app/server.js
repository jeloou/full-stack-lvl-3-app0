var finalhandler = require('finalhandler');
var serveStatic = require('serve-static');
var http = require('http');
var URL = require('url');

// serving static files
var serve = serveStatic('public/', {'index': ['index.html', 'index.htm']});
var todos = [];
var count = 1;

var pattern = /^\/todos\/(\d+)$/;

var app = {
  // create
  post: function(req, res) {
    var body,
	todo;

    res.writeHead(200, {
      'Content-type': 'application/json'
    });

    req.on('data', function(chunk) {
      body = chunk.toString();
      todo = JSON.parse(body);
      todo.id = count++;
      todos.push(todo);

      res.end(JSON.stringify(todo));
    });
  },
  // edit
  put: function(req, res) {
    var url = req.url,
	id;

    id = +pattern.exec(url)[1];
    res.writeHead(200, {
      'Content-type': 'application/json'
    });

    req.on('data', function(chunk) {
      var todo,
	  i;

      body = chunk.toString();
      todo = JSON.parse(body);

      for (i = 0; i < todos.length; i++) {
	if (todos[i].id === id) {
	  break;
	}
      }

      todos[i].title = todo.title;
      todos[i].done = todo.done;

      res.end(JSON.stringify(todos[i]));
    });
  },
  // read
  get: function(req, res) {
    var url = req.url,
	todo,
	id,
	i;

    if (pattern.test(url)) {
      id = +pattern.exec(url)[1];

      res.writeHead(200, {
	'Content-type': 'application/json'
      });

      for (i = 0; i < todos.length; i++) {
	if (todos[i].id === id) {
	  break;
	}
      }

      res.end(JSON.stringify(todos[i]));
      return;
    }

    if (url == '/todos') {
      res.writeHead(200, {
	'Content-type': 'application/json'
      });
      res.end(JSON.stringify(todos));
      return;
    }

    serve(req, res, finalhandler(req, res));
  },
  // remove
  delete: function(req, res) {
    var url = req.url;
    var id = +pattern.exec(url)[1];
    var i;

    for (i = 0; i < todos.length; i++) {
      if (todos[i].id === id) {
	break;
      }
    }

    var todo = todos[i];
    todos.splice(i, 1);
    res.writeHead(200, {
      'Content-type': 'application/json'
    });
    res.end(JSON.stringify(todo));
  }
};

var server = http.createServer(function(req, res) {
  var method = req.method.toLowerCase();
  if (app[method]) {
    return app[method](req, res);
  }

  res.statusCode = 404;
  res.end();
});

module.exports = server;
